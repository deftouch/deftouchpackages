package udpserver

import (
	"net"
)

func (s *Server) receive() {
	defer s.packetConn.Close()
	buffer := make([]byte, s.maxReadbyte)
	for {
		nbytes, addr, err := s.packetConn.ReadFrom(buffer[0:])
		if err != nil {
			continue
		}
		if nbytes > 0 {
			s.enqueue(Message{addr, buffer[0:nbytes]})
		}
	}
}
func (s *Server) enqueue(m Message) {
	s.mq <- m
}

func (s *Server) dequeue() {
	for m := range s.mq {
		go s.onhandleMessage(m.addr, m.msg)
	}
}
func (s *Server) onhandleMessage(addr net.Addr, msg []byte) {
	defer func() {
		s.recover()
	}()
	s.handleIncomingMessage(msg, addr)
}
