package main

import (
	"fmt"

	"bitbucket.org/deftouch/DeftouchPackages/src/memorycache/deftouchmemorycache"
)

func main() {
	cache := deftouchmemorycache.NewCache()
	cache.Set("hello", "world")
	helloData, Exist := cache.Get("hello")
	if Exist {
		fmt.Println("", helloData.(string))

	}

}
