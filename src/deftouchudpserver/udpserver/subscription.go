package udpserver

import (
	"encoding/json"
	"net"
)

func (s *Server) handleUnsubscribe(messageBufer []byte, adress net.Addr) {
	if len(messageBufer) > 4 {
		subscribeInfo := SubscribeInfo{}
		err := json.Unmarshal(messageBufer[4:], &subscribeInfo)
		if err != nil {
			return
		}
		clientinfo, exist := s.GetClientInfo(adress.String() + "roomId")
		if exist {
			savedRoomID := clientinfo.RoomId
			s.memorycache.Delete(adress.String() + "roomId")
			if savedRoomID == subscribeInfo.RoomId {
				roomInfoStructure, exist := s.GetRoomInfo(subscribeInfo.RoomId + "clientinfo_room")
				if exist {
					if _, ok := roomInfoStructure.Addresses[adress.String()]; ok {
						delete(roomInfoStructure.Addresses, adress.String())
					}
					s.SetRoomInfo(subscribeInfo.RoomId+"clientinfo_room", roomInfoStructure)
					go s.sendmessageInRoom(messageBufer, roomInfoStructure.Addresses, subscribeInfo.RoomId)
				}

			}
			s.onClientDisconnected(subscribeInfo, adress.String())
			s.onRoomUnSubscribed(&subscribeInfo, adress.String())
		}

	}
	return
}
func (s *Server) handleSubscribe(messageBufer []byte, adress net.Addr) {
	if len(messageBufer) > 4 {
		subscribeInfo := SubscribeInfo{}
		err := json.Unmarshal(messageBufer[4:], &subscribeInfo)
		if err != nil {
			return
		}
		if len(subscribeInfo.RoomId) > 0 {
			roomInfoStructure := RoomInfo{}
			roomInfoStructure, _ = s.GetRoomInfo(subscribeInfo.RoomId + "clientinfo_room")
			roomInfoStructure.RoomId = subscribeInfo.RoomId
			roomInfoStructure.Addresses[adress.String()] = adress
			s.SetRoomInfo(subscribeInfo.RoomId+"clientinfo_room", roomInfoStructure)
			s.SetClientInfo(adress.String()+"roomId", ClientInfo{subscribeInfo.RoomId})
			s.sendmessageInRoom(messageBufer, roomInfoStructure.Addresses, subscribeInfo.RoomId)
			s.onClientConnected(&subscribeInfo, adress.String())
			s.onRoomSubscribed(&subscribeInfo, adress.String())
		}

	}
	return
}
