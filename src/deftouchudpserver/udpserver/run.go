package udpserver

import (
	"flag"
	"fmt"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
)

func (s *Server) Start() (err error) {
	s.handleProgramExist()
	flag.StringVar(&s.Adress, "addr", s.GetServerIp()+":"+s.GetServerPortString(), "AddressOf the udp Server")
	flag.IntVar(&s.worker, "concurency", runtime.NumCPU()*4, "Number of Worker")

	if s.onServerStarted != nil {
		s.onServerStarted(s.serverIp, strconv.Itoa(s.serverPort))
	}
	err = s.listenAndReceive()
	if err != nil {
		return
	}

	return
}

func (s *Server) listenAndReceive() (err error) {
	s.packetConn, err = net.ListenPacket("udp", s.Adress)
	if err != nil {
		return
	}
	for i := 0; i < s.worker; i++ {
		go s.dequeue()
		if i >= s.worker-1 {
			s.receive()
		} else {
			go s.receive()
		}
	}
	return nil
}

func (s *Server) handleProgramExist() {
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt)
	go func(s *Server) {
		select {
		case sig := <-c:
			s.packetConn.Close()
			if s.onServerShutDown != nil {
				s.onServerShutDown(s.GetServerIp(), s.GetServerPort())
			}
			fmt.Printf("Got %s signal. Aborting...\n", sig.String())

			os.Exit(1)
		}
	}(s)

	return
}
