package goredis

import (
	"context"
	"errors"
	"fmt"
	"log"
	"strconv"
	"time"

	"github.com/go-redis/redis"
)

type NewRedis struct {
	ConnType    string
	IP          string
	Port        string
	UserName    string
	Password    string
	Loglevel    string
	Db          int
	RedisClient *redis.Client
	ConnPtr     *redis.Conn
	IdleTimeout int
}
type user struct {
	ID    string
	Score float64
	Rank  int64
}

var ctx = context.Background()

func (obj *NewRedis) Connect() (err error) {
	obj.RedisClient = redis.NewClient(&redis.Options{
		Addr:        obj.IP + ":" + obj.Port,
		Password:    obj.Password,
		DB:          obj.Db, // use default DB
		IdleTimeout: 240 * time.Second,
	})
	pong, err := obj.RedisClient.Ping(ctx).Result()
	if err != nil {
		log.Println(pong, "Dial Fail", err)
	}
	obj.ConnPtr = obj.GetFromPool()
	return
}

func (obj *NewRedis) GetFromPool() *redis.Conn {
	return obj.RedisClient.Conn(ctx)
}

func (obj *NewRedis) GetInString(key string) (valuee string, exist bool) {
	exist = false
	obj.ConnPtr = obj.GetFromPool()
	defer obj.ConnPtr.Close()
	value := obj.ConnPtr.Get(ctx, key)
	if value.Val() == "" {
		return
	}
	valuee = value.Val()
	exist = true
	return
}
func (obj *NewRedis) SetKey(key string, member string) (err error) {
	obj.ConnPtr = obj.GetFromPool()
	defer obj.ConnPtr.Close()
	status := obj.ConnPtr.Set(ctx, key, member, 0)
	if status.Val() != "OK" {
		err = errors.New("Unable to Set the Key")
	}
	fmt.Println(status.Val())
	return
}
func (obj *NewRedis) SetTimeOut(key string, member string, timeInSecond int) (err error) {
	obj.ConnPtr = obj.GetFromPool()
	defer obj.ConnPtr.Close()
	status := obj.ConnPtr.Set(ctx, key, member, time.Duration(timeInSecond)*time.Second)
	if status.Val() != "OK" {
		err = errors.New("Unable to Set the Key")
	}
	fmt.Println(status)
	return
}
func (obj *NewRedis) IsExist(key string) (Exist bool) {
	obj.ConnPtr = obj.GetFromPool()
	defer obj.ConnPtr.Close()
	Value := obj.ConnPtr.Exists(ctx, key)
	Exist = Value.Val() == 1
	return
}
func (obj *NewRedis) Delete(key string) (err error) {
	obj.ConnPtr = obj.GetFromPool()
	defer obj.ConnPtr.Close()
	Value := obj.ConnPtr.Del(ctx, key)
	if Value.Val() == 0 {
		err = errors.New("Unable to Delete " + key)
	}
	return
}
func (obj *NewRedis) UserRank(key string, member string) (userRank int64) {
	obj.ConnPtr = obj.GetFromPool()
	defer obj.ConnPtr.Close()
	value := obj.ConnPtr.ZRevRank(ctx, key, member)
	userRank = value.Val() + 1
	return
}
func (obj *NewRedis) GetArrayOfValue(key []string) (values []string) {
	obj.ConnPtr = obj.GetFromPool()
	defer obj.ConnPtr.Close()

	values1 := obj.ConnPtr.MGet(ctx, key...)
	for _, data := range values1.Val() {
		if data != nil {
			values = append(values, data.(string))
		}
	}
	return
}
func (obj *NewRedis) GetUserRanks(key string, min int64, max int64) (Users []user) {
	obj.ConnPtr = obj.GetFromPool()
	defer obj.ConnPtr.Close()
	zRevRangeWithScores := obj.ConnPtr.ZRevRangeWithScores(ctx, key, min, max)
	for _, data := range zRevRangeWithScores.Val() {
		member, _ := data.Member.(string)
		min = min + 1

		user := user{ID: member, Score: data.Score, Rank: min}
		Users = append(Users, user)
	}
	return
}
func (obj *NewRedis) ArroundRank(key string, member string, arrountLimit int64) (User []user) {
	userRank := obj.UserRank(key, member)
	fmt.Println(userRank)
	min := userRank - arrountLimit
	max := userRank + arrountLimit
	if min <= 0 {
		min = 0
		max = arrountLimit + arrountLimit
	}
	fmt.Println(userRank, min, max)
	User = obj.GetUserRanks(key, min, max)
	return
}
func (obj *NewRedis) AddOrUpdate(key string, id string, noOfWins string) {
	obj.ConnPtr = obj.GetFromPool()
	defer obj.ConnPtr.Close()
	zAdd := &redis.Z{}
	Score, err := strconv.ParseFloat(noOfWins, 64)
	if err != nil {
		fmt.Println("err addorupdate", err.Error())
		return
	}
	zAdd.Score = Score
	zAdd.Member = id
	value := obj.ConnPtr.ZAdd(ctx, key, zAdd)
	fmt.Println(value.Val())
}
