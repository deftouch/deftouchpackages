package badgerclient

import (
	"time"

	"github.com/dgraph-io/badger"
)

type BadgerCacheCleint struct {
	Cache *badger.DB
}

func Init() (err error, obj *BadgerCacheCleint) {
	obj = &BadgerCacheCleint{}
	opt := badger.DefaultOptions("").WithInMemory(true)
	obj.Cache, err = badger.Open(opt)
	if err != nil {
		return
	}
	return
}
func (obj *BadgerCacheCleint) Set(key string, byteData []byte) bool {
	tx := obj.Cache.NewTransaction(true)
	defer tx.Discard()
	err := tx.Set([]byte(key), byteData)
	if err != nil {
		return false
	}
	err = tx.Commit()
	return true
}
func (obj *BadgerCacheCleint) SetWithTimeOut(key string, byteData []byte, duation time.Duration) bool {
	tx := obj.Cache.NewTransaction(true)
	defer tx.Discard()
	err := tx.SetEntry(badger.NewEntry([]byte(key), byteData).WithTTL(duation))
	if err != nil {
		return false
	}
	err = tx.Commit()
	if err != nil {
		return false
	}
	return true
}

func (obj *BadgerCacheCleint) Get(key string) ([]byte, bool) {
	tx := obj.Cache.NewTransaction(false)
	defer tx.Discard()
	item, _ := tx.Get([]byte(key))
	if item == nil {
		return nil, false

	}
	finalData, err := item.ValueCopy(nil)
	if err != nil {
		return nil, false
	}

	return finalData, true
}
func (obj *BadgerCacheCleint) Delete(key string) {
	tx := obj.Cache.NewTransaction(true)
	defer tx.Discard()
	tx.Delete([]byte(key))
}
