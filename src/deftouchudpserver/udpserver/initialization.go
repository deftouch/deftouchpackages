package udpserver

import (
	"runtime"
	"time"

	"bitbucket.org/deftouch/DeftouchPackages/src/memorycache/deftouchmemorycache"
)

func (s *Server) Initialize() (err error) {
	s.memorycache = deftouchmemorycache.NewCache()
	s.mq = make(messageQueue, 1000000)
	s.timeOut = time.Second * 1800
	s.maxGorutineWorker = runtime.NumGoroutine()
	s.headerByte = 1
	return
}

func (s *Server) AddClientConnectedEvent(typeData interface{}) (err error) {
	s.onClientConnected = typeData.(OnClientConnectedEvent)
	return
}
func (s *Server) AddSubscribeRoomEvent(typeData interface{}) (err error) {
	s.onRoomSubscribed = typeData.(OnRoomSubscribedEvent)
	return
}
func (s *Server) AddUnSubscribeRoomEvent(typeData interface{}) (err error) {
	s.onRoomUnSubscribed = typeData.(OnRoomUnSubscribedEvent)
	return
}
func (s *Server) AddServerReceivedEvent(typeData interface{}) (err error) {
	s.onServerEventReceived = typeData.(OnServerEventReceived)
	return
}
func (s *Server) AddClientDisconnectedEvent(typeData interface{}) (err error) {
	s.onClientDisconnected = typeData.(OnClientDisconnectedEvent)
	return
}
func (s *Server) AddServerShutDownEvent(typeData interface{}) (err error) {
	s.onServerShutDown = typeData.(OnServerShutDownEvent)
	return
}
func (s *Server) AddServerStartedEvent(typeData interface{}) (err error) {
	s.onServerStarted = typeData.(OnServerStartedEvent)
	return
}
func (s *Server) AddForwardMessageEvent(typeData interface{}) (err error) {
	s.onForwardMessage = typeData.(OnForwardMessage)
	return
}
