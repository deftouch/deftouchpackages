package deftouchmemorycache

import (
	"sync"
	"time"
)

type Cache struct {
	mutex      sync.RWMutex
	dataMap    sync.Map
	ttlMap     sync.Map
	isClosed   bool
	maxEntries int64
	expireMq   messageQueue
}
type messageQueue chan string

//#region publick methodes
func NewCache() (cache *Cache) {
	cache = &Cache{}
	cache.isClosed = false
	cache.expireMq = make(messageQueue, 1000000)
	go cache.dequeExpireMq()
	go cache.processttl()
	return
}
func (obj *Cache) Set(key string, value interface{}) {
	obj.dataMap.Store(key, value)
}
func (obj *Cache) SetTTL(key string, value interface{}, ttl time.Duration) {
	expiretime := time.Now().Add(ttl).Unix()
	obj.dataMap.Store(key, value)
	obj.ttlMap.Store(key, expiretime)
}
func (obj *Cache) Get(key string) (interface{}, bool) {
	return obj.dataMap.Load(key)
}
func (obj *Cache) Delete(key string) {
	obj.ttlMap.Delete(key)
	obj.dataMap.Delete(key)
}

//#end region

//#region private mehodes
func (obj *Cache) dequeExpireMq() {
	for {
		for m := range obj.expireMq {
			obj.ttlMap.Delete(m)
			obj.dataMap.Delete(m)
		}

	}
}
func (obj *Cache) enqueue(m string) {
	obj.expireMq <- m
}
func (obj *Cache) processttl() {
	var timeNow int64
	for {
		timeNow = time.Now().Unix()
		obj.ttlMap.Range(func(key, value interface{}) bool {
			if value.(int64) < timeNow {
				obj.enqueue(key.(string))
			}
			return true
		})

		if obj.isClosed {
			break
		}
	}
}

//#end Region
