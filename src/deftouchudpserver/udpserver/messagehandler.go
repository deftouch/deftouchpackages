package udpserver

import (
	"context"
	"net"
	"time"
)

func (s *Server) handleIncomingMessage(messageBufer []byte, adress net.Addr) {
	switch bytesToString(messageBufer[:s.headerByte]) {
	case _subscribe:
		s.handleSubscribe(messageBufer[s.headerByte:], adress)
		break
	case _unSubscribe:
		s.handleUnsubscribe(messageBufer[s.headerByte:], adress)
		break
	case _message:
		s.handleMessage(messageBufer[s.headerByte:], adress)
		break
	default:
		break
	}
	return
}

func (s *Server) recover() {
	if r := recover(); r != nil {
	}
}
func getContext(timeout time.Duration) (ctx context.Context) {
	ctx, _ = context.WithTimeout(context.Background(), timeout)
	return
}
func getContextWithcancel(timeout time.Duration) (ctx context.Context, cancel context.CancelFunc) {
	ctx, cancel = context.WithTimeout(context.Background(), timeout)
	return
}
func handletimeOut(ack chan bool, counter int8) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Microsecond*time.Duration(counter*30))
	defer cancel()
	for {
		select {
		case <-ctx.Done():
			return
		case <-ack:
			counter--
			if counter <= 0 {
				return
			} else {
				continue
			}
		}
	}
}
func (s *Server) sendmessageInRoom(messageBufer []byte, roomInfo map[string]net.Addr, roomid string) {
	ack := make(chan bool)
	defer close(ack)
	var counter int8
	for _, address := range roomInfo {
		counter++
		go func(ac chan bool) {
			defer func() { ac <- true }()
			s.sendmessage(messageBufer, address, roomid)
		}(ack)
	}
	if counter > 0 {
		func(ack chan bool, counter int8) {
			handletimeOut(ack, counter)
		}(ack, counter)
	}
	return
}
func (s *Server) sendmessage(messageBufer []byte, adress net.Addr, roomid string) {
	totalbyteWritten := 0
	totalByte := len(messageBufer)
	tempbuffer := messageBufer
	for {
	write:
		writtensize, err := s.packetConn.WriteTo(tempbuffer, adress)
		if err != nil {
			return
		}
		totalbyteWritten = writtensize + 1
		if totalByte < totalbyteWritten {
			tempbuffer = make([]byte, totalByte-totalbyteWritten)
			copy(messageBufer[(totalbyteWritten+1):], tempbuffer)
			goto write
		}
		break
	}
	return
}
func (s *Server) sendmessageToOpponentsInRoom(messageBufer []byte, roomid string, senderAdress string) {
	roomInfoStructure, exist := s.GetRoomInfo(roomid + "clientinfo_room")
	if exist {
		ack := make(chan bool)
		defer close(ack)
		var counter int8

		for _, address := range roomInfoStructure.Addresses {
			if address.String() != senderAdress {
				go func(ac chan bool) {
					defer func() { ac <- true }()
					s.sendmessage(messageBufer, address, roomid)
				}(ack)

			}
		}
		if counter > 0 {
			func(ack chan bool, counter int8) {
				handletimeOut(ack, counter)
			}(ack, counter)
		}
	}
	return
}
func (s *Server) handleMessage(messageBufer []byte, adress net.Addr) {
	clientinfo, exist := s.GetClientInfo(adress.String() + "roomId")
	if exist {
		if len(clientinfo.RoomId) > 0 {
			s.sendmessageToOpponentsInRoom(messageBufer, clientinfo.RoomId, adress.String())
		}
	}
	return
}
