package sql

import (
	"database/sql"
	"errors"
	"log"
	"time"

	"sync"

	_ "github.com/go-sql-driver/mysql"
)

var mutex = &sync.Mutex{}

type Database struct {
	Username    string
	Password    string
	IP          string
	Port        string
	Type        string
	Loglevel    string
	Schema      string
	ReplicaType string
	ConnPtr     *sql.DB
}

type Transaction struct {
	Txn      *sql.Tx
	Loglevel string
}

func (obj *Database) Connect() (err error) {
	conn_string := ""
	if obj.Type == "mysql" {
		conn_string = obj.Username + ":" + obj.Password + "@tcp(" + obj.IP + ":" + obj.Port + ")/" + obj.Schema
	} else if obj.Type == "postgres" {
		conn_string = obj.Type + "://" + obj.Username + ":" + obj.Password + "@" + obj.IP + ":" + obj.Port + "/" + obj.Schema + "?sslmode=disable"
	} else {
		err = errors.New("Unsupported DB Type : Check with Admin for DB Info")
	}
	obj.ConnPtr, err = sql.Open(obj.Type, conn_string)

	if err != nil {
		log.Println(obj.Loglevel, "Error", err)
		err = errors.New("DB connect fail")
		return
	}
	log.Println("Debug", "Info", "obj.IP ", obj.IP, "obj.Username", obj.Username, "obj.Password", obj.Password)
	time.Sleep(time.Millisecond * time.Duration(200))
	if obj.Type == "mysql" {
		err = obj.ConnPtr.Ping()
		if err != nil {
			log.Println("Error", "Error", err)
			err = errors.New("DB ping fail")
			obj.ConnPtr.Close()
			return
		}
	}
	obj.ConnPtr.SetConnMaxLifetime(time.Second * 600)

	return
}

func (obj *Database) DisConnect() (err error) {
	err = obj.ConnPtr.Close()
	if err != nil {
		log.Println(obj.Loglevel, "Error", err)
		err = errors.New("DB Disconnect fail")
		return
	}
	return
}

func (obj *Database) Query(query string, args ...interface{}) (row *sql.Rows, err error) {
	if obj.Type == "mysql" {
		err = obj.ConnPtr.Ping()
		if err != nil {
			log.Println(obj.Loglevel, "Error", err)
			err = errors.New("DB ping fail")
			return
		}
	}
	log.Println(obj.Loglevel, "Debug", "Query : ", query)
	log.Println(obj.Loglevel, "Debug", "Query Args : ", args)

	row, err = obj.ConnPtr.Query(query, args...)
	if err != nil {
		log.Println(obj.Loglevel, "Error", err)
		err = errors.New(query + "DB query fail")
		return
	}
	return
}

func (obj *Database) Exec(query string, args ...interface{}) (result sql.Result, err error) {
	if obj.Type == "mysql" {
		err = obj.ConnPtr.Ping()
		if err != nil {
			log.Println("Error", "Error", err)
			err = errors.New("DB ping fail")
			return
		}
	}
	log.Println(obj.Loglevel, "Debug", "Query : ", query)
	log.Println(obj.Loglevel, "Debug", "Query Args : ", args)

	result, err = obj.ConnPtr.Exec(query, args...)

	if err != nil {
		log.Println(obj.Loglevel, "Error", err)
		err = errors.New(query + "DB exec fail")
		return
	}
	return
}

func Scan(row *sql.Rows) (cols []string, data [][]string, err error) {
	cols, err = row.Columns()
	if err != nil {
		err = errors.New("DB get columns fail")
		return
	}

	tmp_byte := make([][]byte, len(cols))
	tmp := make([]interface{}, len(cols))
	for i, _ := range tmp_byte {
		tmp[i] = &tmp_byte[i] // Put pointers to each string in the interface slice
	}
	for row.Next() {
		err = row.Scan(tmp...)
		if err != nil {
			err = errors.New("DB row scan fail")
			return
		}
		rawResult := make([]string, len(cols))
		for i, _ := range tmp_byte {
			rawResult[i] = string(tmp_byte[i]) // Put pointers to each string in the interface slice
		}
		data = append(data, rawResult)
	}
	return
}

func Close(row *sql.Rows) (err error) {
	if row != nil {
		row.Close()
	}
	return
}

func (obj *Database) Begin() (tx Transaction, err error) {
	if obj.Type == "mysql" {
		err = obj.ConnPtr.Ping()
		if err != nil {
			log.Println(tx.Loglevel, "Error", err)
			err = errors.New("DB ping fail")
			return
		}
	}
	tx.Txn, err = obj.ConnPtr.Begin()
	if err != nil {
		log.Println(tx.Loglevel, "Error", err)
		err = errors.New("Tx Begin fail")
		return
	}
	return
}

func (tx *Transaction) Commit() (err error) {
	err = tx.Txn.Commit()
	if err != nil {
		log.Println(tx.Loglevel, "Error", err)
		err = errors.New("Tx Commit fail")
		return
	}
	return
}

func (tx *Transaction) Rollback() (err error) {
	err = tx.Txn.Rollback()
	if err != nil {
		log.Println(tx.Loglevel, "Error", err)
		err = errors.New("Tx Rollback fail")
		return
	}
	return
}

func (tx *Transaction) Exec(query string, args ...interface{}) (result sql.Result, err error) {
	log.Println(tx.Loglevel, "Debug", "Query : ", query)
	log.Println(tx.Loglevel, "Debug", "Query Args : ", args)
	result, err = tx.Txn.Exec(query, args...)
	if err != nil {
		log.Println(tx.Loglevel, "Error", err)
		err = errors.New(query + "Tx exec fail")
		return
	}
	return
}

func (tx *Transaction) Query(query string, args ...interface{}) (row *sql.Rows, err error) {
	log.Println(tx.Loglevel, "Debug", "Query : ", query)
	log.Println(tx.Loglevel, "Debug", "Query Args : ", args)
	row, err = tx.Txn.Query(query, args...)
	if err != nil {
		log.Println(tx.Loglevel, "Error", err)
		err = errors.New(query + "Tx query fail")
		return
	}
	return
}
func (obj *Database) Prepare(query string) (stmnt *sql.Stmt, err error) {

	log.Println(obj.Loglevel, "Debug", "Query : ", query)
	stmnt, err = obj.ConnPtr.Prepare(query)
	if err != nil {
		log.Println(obj.Loglevel, "Error", err)
		return
	}
	return
}
