package udpserver

import (
	"net"
	"strconv"
)

func (s *Server) SetEndPoint(ip string, port int) {
	s.serverIp = ip
	s.serverPort = port
	return
}
func (s *Server) GetServerIp() string {
	return s.serverIp

}
func (s *Server) SetMaxReadByte(size int) {
	s.maxReadbyte = size
}
func (s *Server) SetTimeout(timeoutInSecond int) {
	s.timeoutInSecond = timeoutInSecond
}
func (s *Server) GetServerPort() int {
	return s.serverPort
}
func (s *Server) GetServerPortString() string {
	return strconv.Itoa(s.serverPort)
}
func (s *Server) GetNumberOfConnection() int {
	return s.numberofConnection
}
func (s *Server) getServerConn() *net.UDPConn {
	return s.udpConn
}
