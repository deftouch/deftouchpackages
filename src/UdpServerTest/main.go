package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net"
	"os"
	"sync"
)

func main() {
	udpInfoMessage = make(map[string]ResponseRelayServer)
	udpaddr, err := net.ResolveUDPAddr("udp", "0.0.0.0"+":"+"10000")
	if err != nil {
		log.Println("Error", err.Error())
		os.Exit(1)
	}
	packetConn, err := net.ListenUDP("udp", udpaddr)
	if err != nil {
		return
	}
	log.Println("Info", "UDP Game server running on", "0.0.0.0"+":"+"10000")
	for {
		rm := make([]byte, 1024)
		rBytes, rmaddr, err := packetConn.ReadFromUDP(rm)
		if err != nil {
			log.Println("Debug", err.Error())
			continue
		}
		log.Println("Info", "Request Packet Size is in udp", rBytes, "Bytes")
		rmf := make([]byte, rBytes)
		copy(rmf, rm)
		log.Println("Info", "Request message in udp", string(rmf[:]))
		go onHandleRequest(rmf[1:], rmaddr, packetConn)
	}
}

type RequestRelayServer struct {
	Id        string `json:"Id"`
	LocalIp   string `json:"LocalIp"`
	LocalPort int    `json:"LocalPort"`
}
type ResponseRelayServer struct {
	Id         string `json:"Id"`
	LocalIp    string `json:"LocalIp"`
	LocalPort  int    `json:"LocalPort"`
	PublicIp   string `json:"PublicIp"`
	PublicPort int    `json:"PublicPort"`
}

var rwMutex sync.RWMutex
var udpInfoMessage map[string]ResponseRelayServer

func onHandleRequest(message []byte, connptr *net.UDPAddr, udpconn *net.UDPConn) {
	requestRelayServer := RequestRelayServer{}
	err := json.Unmarshal(message, &requestRelayServer)
	if err == nil {
		fmt.Println("onHandleRequest", requestRelayServer)
		ResponseRelayServer := ResponseRelayServer{}
		ResponseRelayServer.Id = requestRelayServer.Id
		ResponseRelayServer.LocalIp = requestRelayServer.LocalIp
		ResponseRelayServer.LocalPort = requestRelayServer.LocalPort
		ResponseRelayServer.PublicIp = connptr.IP.String()
		ResponseRelayServer.PublicPort = connptr.Port
		udpInfoMessage[requestRelayServer.Id] = ResponseRelayServer
		patnerId := "1"
		if requestRelayServer.Id == "1" {
			patnerId = "2"
		}
		if value, ok := udpInfoMessage[patnerId]; ok {
			messagebyte, _ := json.Marshal(value)
			udpconn.WriteToUDP(getMessage(messagebyte), connptr)
		}

	} else {
		fmt.Println("onHandleRequest err ", err)

	}
	return
}

func getMessage(message []byte) []byte {
	return append([]byte("1"), message...)
}
