package commonmodel

//Db Credential Structure
//json Structure ///{"UserName":"root","Ip":"65.0.219.148","Port":"3306","Type":"mysql","Schema":"conf_db"}
type DbCredential struct {
	UserName string `json:"UserName"`
	Ip       string `json:"Ip"`
	Port     string `json:"Port"`
	Type     string `json:"Type"`
	Schema   string `json:"Schema"`
	Password string `json:"Password"`
}

//Redis Credential structure
//{"Type":"tcp","IP":"ec2-15-207-217-27.ap-south-1.compute.amazonaws.com","Port":"6380","Name":"redis-cache","Username":"","Password":"deftouch7","RedisIdleTimeOut":15,"Db":"1"}
type RedisCredential struct {
	Type             string `json:"Type"`
	IP               string `json:"IP"`
	Port             string `json:"Port"`
	Name             string `json:"Name"`
	Username         string `json:"Username"`
	Password         string `json:"Password"`
	RedisIdleTimeOut int    `json:"RedisIdleTimeOut"`
	Db               string `json:"Db"`
}
type RequestConfFile struct {
	ServiceId int32 `json:"ServiceId"`
}
type ServiceConfigRules struct {
	LogoIdRules      LogoIdRules      `json:"LogoIdRules,omitempty"`
	JersyRules       JersyRules       `json:"JersyRules,omitempty"`
	GameRules        GameRules        `json:"GameRules,omitempty"`
	RandomGenMinMax  RandomGenMinMax  `json:"RandomGenMinMax,omitempty"`
	SocialRules      SocialRules      `json:"SocialRules,omitempty"`
	LeaderboardRules LeaderboardRules `json:"LeaderboardRules,omitempty"`
}
type LogoIdRules struct {
	MinimumId int `json:"MinimumId,omitempty"`
	MaximumId int `json:"MaximumId,omitempty"`
}
type JersyRules struct {
	MinimumId int `json:"MinimumId,omitempty"`
	MaximumId int `json:"MaximumId,omitempty"`
}
type GameRules struct {
	FirstTimeUserCoinReward string `json:"FirstTimeUserCoinReward"`
	FirstTimeUserGemsReward string `json:"FirstTimeUserGemsReward,omitempty"`
	FirstTimeUserTrophy     string `json:"FirstTimeUserTrophy,omitempty"`
	FirstTimeUserTrophyInt  int    `json:"FirstTimeUserTrophyInt,omitempty"`
}
type SocialRules struct {
	MaxNumberOfFriendList int `json:"MaxNumberOfFriendList,omitempty"`
}
type LeaderboardRules struct {
	MiniLeaderboardLimit         int `json:"MiniLeaderboardLimit,omitempty"`
	BigLeaderboardTopLimit       int `json:"BigLeaderboardTopLimit,omitempty"`
	BigLeaderboardArroundMeLimit int `json:"BigLeaderboardArroundMeLimit,omitempty"`
}

type RandomGenMinMax struct {
	Minimum int `json:"Minimum,omitempty"`
	Maximum int `json:"Maximum,omitempty"`
}
type ReferralRewards struct {
	Reward1        []Reward `json:"reward_1"`
	Reward2        []Reward `json:"reward_2"`
	Reward3        []Reward `json:"reward_3"`
	ReferralReward []Reward `json:"referral_reward"`
}

type Reward struct {
	Item         string `json:"item"`
	QuantityOrId int    `json:"quantity_or_id"`
}
