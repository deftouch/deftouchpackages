package udpserver

import (
	"net"
	"sync"
	"time"

	"bitbucket.org/deftouch/DeftouchPackages/src/memorycache/deftouchmemorycache"
)

type RoomInfo struct {
	RoomId    string
	Addresses map[string]net.Addr
}
type ClientInfo struct {
	RoomId string
}
type SubscribeInfo struct {
	Name   string `json:"name"`
	Id     string `json:"id"`
	RoomId string `json:"roomId"`
}
type OnServerEventReceived = func(cinetAdress string, message []byte)
type OnRoomSubscribedEvent = func(client *SubscribeInfo, cinetAdress string)
type OnRoomUnSubscribedEvent = func(client *SubscribeInfo, cinetAdress string)
type OnClientDisconnectedEvent = func(client SubscribeInfo, cinetAdress string)
type OnServerStartedEvent = func(ip string, port string)
type OnClientConnectedEvent = func(client *SubscribeInfo, cinetAdress string)
type OnServerShutDownEvent = func(ip string, port int)
type OnForwardMessage = func(senderClient string, receiverClient string, message []byte)
type messageQueue chan Message

var (
	_subscribe   = "0"
	_message     = "1"
	_unSubscribe = "2"
)

type Msg struct {
	content []byte
}
type Message struct {
	addr net.Addr
	msg  []byte
}

type Server struct {
	headerByte            int8
	mq                    messageQueue
	bufferPool            sync.Pool
	waitGroup             sync.WaitGroup
	memorycache           *deftouchmemorycache.Cache
	serverIp              string
	serverPort            int
	numberofConnection    int
	udpConn               *net.UDPConn
	packetConn            net.PacketConn
	maxGorutineWorker     int
	Adress                string
	mutex                 sync.RWMutex
	maxReadbyte           int
	timeoutInSecond       int
	worker                int
	onClientConnected     OnClientConnectedEvent
	onServerStarted       OnServerStartedEvent
	onServerShutDown      OnServerShutDownEvent
	onClientDisconnected  OnClientDisconnectedEvent
	onRoomSubscribed      OnRoomSubscribedEvent
	onRoomUnSubscribed    OnRoomUnSubscribedEvent
	onServerEventReceived OnServerEventReceived
	onForwardMessage      OnForwardMessage
	timeOut               time.Duration
}
