package ristrettoclient

import (
	"fmt"
	"time"

	"github.com/dgraph-io/ristretto"
)

type RistrettoCache struct {
	Cache *ristretto.Cache
}

func (obj *RistrettoCache) Init() (err error) {
	obj = &RistrettoCache{}
	obj.Cache, err = ristretto.NewCache(&ristretto.Config{
		NumCounters: 1e7,     // number of keys to track frequency of (10M).
		MaxCost:     1 << 30, // maximum cost of cache (1GB).
		BufferItems: 64,      // number of keys per Get buffer.
	})
	if err != nil {
		return
	}
	fmt.Println("Init()", obj.Cache)
	return
}
func (obj *RistrettoCache) Set(key string, value interface{}) bool {
	return obj.Cache.Set(key, value, 1)

}
func (obj *RistrettoCache) SetTimeOut(key string, value interface{}, timeOutInSecond int) bool {
	return obj.Cache.SetWithTTL(key, value, 1, (time.Duration)(timeOutInSecond)*time.Second)
}
func (obj *RistrettoCache) Get(key string) (interface{}, bool) {
	return obj.Cache.Get(key)
}
func (obj *RistrettoCache) Delete(key string) {
	obj.Cache.Del(key)
}
