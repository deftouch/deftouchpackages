package udpserver

func (s *Server) GetClientInfo(key string) (clientinfo ClientInfo, exist bool) {

	clientinfoInterface, ok := s.memorycache.Get(key)
	if ok {
		clientinfo = clientinfoInterface.(ClientInfo)
		exist = true
	}
	return
}
func (s *Server) GetRoomInfo(key string) (roomInfo RoomInfo, exist bool) {
	roomInfoInterface, ok := s.memorycache.Get(key)
	if ok {
		roomInfo = roomInfoInterface.(RoomInfo)
		exist = true
	}
	return
}
func (s *Server) SetClientInfo(key string, clientinfo ClientInfo) {
	s.memorycache.SetTTL(key, clientinfo, s.timeOut)
	return
}
func (s *Server) SetRoomInfo(key string, roomInfo RoomInfo) {
	s.memorycache.SetTTL(key, roomInfo, s.timeOut)
	return
}
