package main

import (
	"fmt"

	"deftouchudpserver/udpserver"
)

var UdpServer udpserver.Server

func main() {

	err := UdpServer.Initialize()
	if err != nil {
		fmt.Println("Unable initialized Seever with error ", err)
		return
	}

	UdpServer.SetEndPoint("127.0.0.1", 5080)
	UdpServer.SetMaxReadByte(1024)
	UdpServer.SetTimeout(240)
	UdpServer.AddServerReceivedEvent(OnMessageReceived)
	UdpServer.AddClientDisconnectedEvent(OnClientDisconnected)
	UdpServer.AddClientConnectedEvent(OnClientConnected)
	UdpServer.AddServerShutDownEvent(OnServerShutDownEvent)
	UdpServer.AddServerStartedEvent(OnServerStarted)
	UdpServer.AddSubscribeRoomEvent(OnRoomSubscribed)
	UdpServer.AddUnSubscribeRoomEvent(OnRoomUnSubscribed)
	UdpServer.AddForwardMessageEvent(OnForwardMessage)
	UdpServer.Start()
}
func OnMessageReceived(cinetAdress string, message []byte) {
	fmt.Println("Server shutdown succesfully")

}
func OnClientDisconnected(client udpserver.SubscribeInfo, cinetAdress string) {
	fmt.Println("Server shutdown succesfully")

}
func OnClientConnected(client *udpserver.SubscribeInfo, cinetAdress string) {
	fmt.Println("Server shutdown succesfully")

}
func OnServerShutDownEvent(serverIp string, port int) {
	fmt.Println("Server shutdown succesfully")
}

func OnRoomSubscribed(client *udpserver.SubscribeInfo, cinetAdress string) {
	fmt.Println("Server shutdown succesfully")

}
func OnRoomUnSubscribed(client *udpserver.SubscribeInfo, cinetAdress string) {
	fmt.Println("Server shutdown succesfully")

}
func OnServerStarted(ip string, port string) {
	fmt.Println("Udp Server Listening  with ip ", ip, "port", port)

}
func OnForwardMessage(senderClient string, receiverClient string, message []byte) {
	fmt.Println("Server shutdown succesfully")
}
