package main

import (
	"bufio"
	"fmt"
	"net"
)

type SubscribeInfo struct {
	Name   string `json:"name"`
	Id     string `json:"id"`
	RoomId string `json:"roomId"`
}

func main() {
	p := make([]byte, 2048)
	conn, err := net.Dial("udp", "127.0.0.1:5080")
	if err != nil {
		fmt.Printf("Some error %v", err)
		return
	}
	conn.Write([]byte("Hi UDP Server, How are you doing?"))
	fmt.Println(conn, "Hi UDP Server, How are you doing?")
	_, err = bufio.NewReader(conn).Read(p)
	if err == nil {
		fmt.Printf("%s\n", p)
	} else {
		fmt.Printf("Some error %v\n", err)
	}
	conn.Close()
}

func processClient() {
	p := make([]byte, 2048)
	conn, err := net.Dial("udp", "127.0.0.1:5080")
	if err != nil {
		fmt.Printf("Some error %v", err)
		return
	}
	defer conn.Close()
	//read client
	go func() {
		_, err = bufio.NewReader(conn).Read(p)
		if err == nil {
			fmt.Printf("%s\n", p)
		} else {
			fmt.Printf("Some error %v\n", err)
		}
	}()
}
